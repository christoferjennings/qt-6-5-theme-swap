#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QTranslator>
#include <QtQml>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.addImportPath(":/myapp/swap");
    const QUrl url(u"qrc:/myapp/swap/main.qml"_qs);
    engine.load(url);

    return app.exec();
}
