import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Window {
    id: window
    width: 500
    height: 300
    visible: true
    title: qsTr("Qt 6.5 QML Theme Swap")

    ThemeManager{
        id: tm
    }

    Rectangle {
        anchors.fill: parent
        color: tm.theme.aColor

        Button {
            id: theButton
            text: "swap"
            anchors.centerIn: parent
            onClicked: tm.swap()
        }
    }
}