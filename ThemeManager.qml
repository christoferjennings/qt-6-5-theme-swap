import QtQuick

QtObject {
    id: mgr

    property Loader loader: Loader {
        source: "BlueTheme.qml"
    }

    property QtObject theme: loader.item

    function swap() {
        if (loader.source == "BlueTheme.qml") {
            loader.source = "GreenTheme.qml"
        } else {
            loader.source = "BlueTheme.qml"
        }
    }
}